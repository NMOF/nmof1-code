% returns.m -- version 2010-09-11
%% generate artificial price data: 
% R = returns, P = prices
ns = 100;  % number of scenarios
na = 10;   % number of assets
R  = 1 + randn(ns, na) * 0.01;
P  = cumprod( [100 * ones(1, na); R] );
plot(P);

%% discrete returns
% compute returns: rets should be equal to R
rets1 = P(2:end,:) ./ P(1:(end-1),:) ;
% ... or
rets2 = diff(P) ./ P(1:(end-1),:) + 1; 
max(max(abs(rets1-R)))  % not 'exactly' equal
max(max(abs(rets2-R)))  % not 'exactly' equal
max(max(abs(rets1-rets1)))  % 'exactly' equal

%% log-returns
rets3 = diff(log(P));
% ... almost like discrete returns
plot(rets1(:) - rets3(:) - 1)