function y1 = Broyden(f,y1,B,tol,itmax)
% Broyden.m  -- version  2010-08-10
n = numel(y1);
if nargin < 3, B = eye(n,n); tol = 1e-2; itmax = 10; end
y1 = y1(:); y0 = -y1;  k = 0; 
F1 = feval(f,y1);
while ~converged(y0,y1,tol)
    y0 = y1;
    F0 = F1;
    s = B \ -F0;
    y1 = y0 + s;
    F1 = feval(f,y1);
    dF = F1 - F0;
    B = B + ((dF - B*s)*s')/(s'*s);
    k = k + 1;
    if k > itmax, error('Iteration limit reached'); end
end
