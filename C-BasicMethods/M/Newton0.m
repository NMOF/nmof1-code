function x1 = Newton0(f,x1,tol)
% Newton0.m  -- version  2010-03-30
% Newton method with numerical derivation
if nargin == 2, tol = 1e-8; end
it = 0; itmax = 10; x0 = realmax; h = 1e-8;
while ~converged(x0,x1,tol)
   x0 = x1;
   f0 = feval(f,x0);
   f1 = feval(f,x0 + h);
   df = (f1 - f0) / h;
   x1 = x0 - f0/df;
   it = it + 1;
   if it > itmax, error('Maxit in Newton0'); end
end