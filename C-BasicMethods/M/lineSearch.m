function a1 = lineSearch(f,x,s,d)
%  lineSearch.m  -- version 2010-09-13
if nargin == 3, d = 0.1; end
done = 0; a1 = 0; k = 0; 
f1 = feval(f,x);
while ~done
    a0 = a1;  a1 = a0 + d;
    f0 = f1;  f1 = feval(f,x+a1*s);
    if f1 > f0, done = 1; end
    k = k + 1;
    if k > 100, fprintf('Early stop in lineSearch'); break, end
end
