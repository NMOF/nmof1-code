function x1 = FPI(f,x1,tol)
% FPI.m  -- version 2008-05-08
% FPI(f,x0)  Fixed point iteration
if nargin == 2, tol = 1e-6; end
it = 0; itmax = 200; x0 = realmax;
while ~converged(x0,x1,tol)
   x0 = x1;
   x1 = feval(f,x0);
   it = it + 1;
   if it > itmax, error('Maxit in FPI'); end
end

