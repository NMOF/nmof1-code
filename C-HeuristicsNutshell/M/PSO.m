function output = PSO(PS,OF,Data)
% PSO.m  -- version  2011-01-09
nP = PS.nP;  nG = PS.nG;  d = Data.d;  int = Data.int;
FF = zeros(nG,1);  F = zeros(nP,1);  P = zeros(nP,d);
for i = 1:d
    P(:,i) = int(i,1) + (int(i,2)-int(i,1)) * rand(nP,1);
end
v = PS.cv * rand(nP,d);
for i = 1:nP
    F(i) = feval(OF,P(i,:),Data);
end
Pbest = P;  Fbest = F;  
[Gbest,gbest] = min(F);
for k = 1:nG
    v = v + PS.c1* rand(nP,d) .* (Pbest     - P) + ...
            PS.c2* rand(nP,d) .* (ones(nP,1)*Pbest(gbest,:)-P);
    v = min(v, PS.vmax);
    v = max(v,-PS.vmax);
    P = P + v;
    for i = 1:nP
        F(i) = feval(OF,P(i,:),Data);
    end
    I = find(F < Fbest);
    if ~isempty(I)
        Fbest(I)   = F(I);
        Pbest(I,:) = P(I,:);
        [Fmin,ib] = min(F(I));
        if Fmin < Gbest
            Gbest = Fmin;  gbest = I(ib);  FF(k) = Gbest;
        end
    end
end
output.xbest = Pbest(gbest,:);
output.Fbest = Gbest; output.FF = FF;


