function P = StartingPop(GA, OF, Data)
% StartingPop.m  -- version 2007-02-11
Imax = 2^GA.nC - 1 ;
Pd = unidrnd(Imax, GA.nP, 1) ;
for i = 1:GA.nP
	P.C{i} = dec2bin(Pd(i), GA.nC) ;
	P.F(i) = feval(OF, P.C{i}, Data) ; % Compute fitness
end
