clear variables, close all hidden, clc
% rand('seed',23246); randn('seed',886471);  % only for demonstration
% rand('seed',93247); randn('seed',386475);  % only for demonstration

%OF = 'Ackley'; Data = struct('int',[-2 2; -2 2],'d',2);
%OF = 'BitString'; Data = struct('X',2^50+2^25+2^20+2^5+2^0,'nc',52);
%NF = 'DNeighbor';
%Data = DStartingSol(OF,Data);

% goTA.m  -- version 2007-11-12
OF = 'Shekel'; Data = struct('int',[0 10; 0 10],'d',2,'m',10);
NF = 'CNeighbor';
TA = struct('Restarts',9,'Rounds',5,'Steps',4000,...
            'ptrim',0.1,'frac',0.2);
TA.Percentiles = linspace(0.9,0,TA.Rounds);

Data = StartingSol(OF,Data);
output = thSequence(TA,OF,NF,Data);    
TA.th = output.th;
for r = 1:TA.Restarts
    Data = StartingSol(OF,Data);
    output = TAH(TA,OF,NF,Data);
    Sol(r) = output.Fbest;
    X(r,:) = output.xbest;
    plotTA(TA,output)
end
[Sbest,i] = min(Sol);
fprintf('\n Sol = %6.3f  ',Sbest); disp(X(i,:));

figure(2), subplot(211)
H = cdfplot(Sol); 
xlabel(''); ylabel(''); title('');
set(H,'Color',.7*[1 1 1],'LineWidth',3), set(gca,'FontSize',10)

% print -depsc ..\Figs\M-TA-cdf00.eps