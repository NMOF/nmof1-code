function output = DEH(DE,OF,Data)
% DEH.m  -- version 2012-04-11
% --Construct starting population
P1 = zeros(Data.d,DE.nP);
Pu = zeros(Data.d,DE.nP);
for i = 1:Data.d
    P1(i,:) = Data.int(i,1) + (Data.int(i,2)-Data.int(i,1))*...
              rand(DE.nP,1);
end
Fbv   = NaN(DE.nG,1);
Fbest = realmax;
F = zeros(DE.nP,1);
for i = 1:DE.nP
    F(i) = feval(OF,P1(:,i)',Data);
    if F(i) < Fbest
        Fbest = F(i);   xbest = P1(:,i);
    end
end
Col = 1:DE.nP;
for k = 1:DE.nG
    P0 = P1;
    Io = randperm(DE.nP)';
    Ic = randperm(4)';
    I  = circshift(Io,Ic(1));
    R1 = circshift(Io,Ic(2));
    R2 = circshift(Io,Ic(3));
    R3 = circshift(Io,Ic(4));
    % --Construct mutant vector
    Pv = P0(:,R1) + DE.F * (P0(:,R2) - P0(:,R3));
    % --Crossover
    mPv = rand(Data.d,DE.nP) < DE.CR;
    if DE.oneElemfromPv
       Row = unidrnd(Data.d,1,DE.nP);
       mPv1 = sparse(Row,Col,1,Data.d,DE.nP);
       mPv = mPv | mPv1;
    end
    mP0  = ~mPv;
    Pu(:,I) = P0(:,I).*mP0 + mPv.*Pv;
    % --Select vector to enter new generation
    flag = 0;
    for i = 1:DE.nP
        Ftemp = feval(OF,Pu(:,i)',Data);
        if Ftemp <= F(i)
            P1(:,i) = Pu(:,i);
            F(i) = Ftemp;
            if Ftemp < Fbest
                Fbest = Ftemp;  xbest = Pu(:,i);  flag = 1;
            end
        else
            P1(:,i) = P0(:,i);
        end
    end
    if flag, Fbv(k) = Fbest; end
end
output.Fbest = Fbest;  output.xbest = xbest;  output.Fbv = Fbv;
