function plotPS(PS,output)
% plotDE.m  -- version 2011-01-08
clf(gcf), figure(1), subplot(212)
semilogy(1:PS.nG,output.FF,'ko','MarkerSize',8,...
         'MarkerFaceColor',.7*[1 1 1]);
set(gca,'FontSize',10); grid on; xlabel('Generations');
