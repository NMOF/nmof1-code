function F = BitString(x,Data)
% BitString.m  -- version 2011-01-08
F = abs( bin2dec(x) - Data.X );