function P1 = MatingPool(GA,P)
% MatingPool.m  -- version 2001-02-11
IP1 = unidrnd(GA.nP,1,GA.nP1); % Set of indices defining P1
for k = 1:GA.nP1
	P1.C{k} = P.C{IP1(k)};
end
P1.F = P.F(IP1);
