function Data = DStartingSol(OF,Data)
% DStartingSol.m  -- version 2011-01-09
Imax = 2^Data.nc - 1 ;
k = unidrnd(Imax,1) ;
Data.xs = dec2bin(k,Data.nc) ;
Data.Fs = feval(OF,Data.xs,Data);
end
