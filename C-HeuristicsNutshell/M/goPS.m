clear variables, close all hidden
%rand('state',10000); 
%OF = 'Shekel'; Data = struct('int',[0 10; 0 10],'d',2,'m',10);

% goDE.m  -- version 2007-01-08
OF = 'Ackley'; Data = struct('int',[-2 2; -2 2],'d',2);

PS = struct('nP',15,'nG',50,'c1',2,'c2',2,'cv',1,'vmax',1,...
            'Restarts',20);
tic
for r = 1:PS.Restarts
    output = PSO(PS,OF,Data);
    Sol(r) = output.Fbest;
    X(r,:) = output.xbest;
    plotPS(PS,output);
end
[Sbest,i] = min(Sol);
fprintf('\n Sol = %6.3f (%i sec)',Sbest,fix(toc));disp(X(i,:));

figure(1), subplot(211)
H = cdfplot(Sol);   
xlabel(''); ylabel(''); title('');
set(H,'Color',.7*[1 1 1],'LineWidth',3), set(gca,'FontSize',10)

% print -depsc ..\Figs\M-PSplotS.eps
