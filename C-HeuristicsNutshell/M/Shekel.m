function F = Shekel(x,Data)
% Shekel.m  -- version 2011-01-08
d = Data.d;  m = Data.m;
if d > 4; error('More than 4 dimensions'); end
if nargin==1, m=10; elseif m>10||m<2, error('Wrong m');else end
A = [4 4 4 4;  1 1 1 1;  8 8 8 8;  6 6 6 6;  3 7.0 3 7.0;
     2 9 2 9;  5 5 3 3;  8 1 8 1;  6 2 6 2;  7 3.6 7 3.6];
c = [0.1 0.2 0.2 0.4 0.4 0.6 0.3 0.7 0.5 0.5];
%
F = 0;
for i = 1:m
    for j = 1:d
        c(i) = c(i) + (x(j) - A(i,j))^2;
    end
    F = F - 1 / c(i);
end
