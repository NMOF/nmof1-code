function plotTA(TA,output)
% plotTA.m  -- version 2011-08-13
figure(1), subplot(212) 
iup = output.iup;
plot(output.FF(100:iup),'k-'), hold on  % title('Obj. func.')
ymax = max(output.FF(1:iup)); ymin = min(output.FF(1:iup));
for i = 1:length(output.uprounds)
    x = output.uprounds(i);
    plot([x x],[ymin ymax],'k:');
end
ylim([ymin ymax]); 

