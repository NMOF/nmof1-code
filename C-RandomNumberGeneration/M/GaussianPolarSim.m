function [z1,z2] = GaussianPolarSim(N_samples)
% GaussianPolarSim.m  -- version 2011-01-06

if nargin < 1, N_samples = 1; end
a = rand(N_samples,2) * 2 - 1;
s = a(:,1).^2 + a(:,2).^2;

% --check for "at-or-within-unit-circle" criterion
outside = s > 1;
while sum(outside) > 0
    a(outside,:) = rand(sum(outside),2) * 2 - 1;
    s(outside,:) = a(outside,1).^2 + a(outside,2).^2;
    outside = s > 1;
end

% --perform transformation
f  = sqrt(-2*log(s)./s);
z1 = f .* a(:,1);
z2 = f .* a(:,2);

