function Xs = TaylorThompson(X,N,m)
% TaylorThompson.m  -- version 2011-01-06
%   X ... original sample
%   N ... number of new samples to be drawn
%   m ... number of neighbors to be used

[xr xc] = size(X);
% -- compute Euclidean distances
B = X * X';
ED = sqrt(repmat(diag(B),1,xr) + repmat(diag(B)',xr,1) - 2*B);

% -- limits for weights
m = min(xr,m);
uLim = 1/m + [-1 1] * sqrt(3*(m-1)/m.^2);

% -- draw samples
Xs = zeros(N,xc);
for s = 1:N
    j = ceil(rand*xr);
    % -- m nearest neighbors:
    [dnn inn] = sort(ED(:,j));
    Xnn = X(inn(1:m),:);
    % -- weights
    u = rand(1,m) * (uLim(2)-uLim(1)) + uLim(1);
    % -- form linear combinations
    Xnn_bar = ones(1,m) * Xnn  / m;
    e = u * (Xnn - repmat(Xnn_bar,m,1));
    Xs(s,:) = Xnn_bar + e;
end
