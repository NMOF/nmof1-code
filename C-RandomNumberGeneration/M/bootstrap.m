function x = bootstrap(r,n,b)
% bootstrap.m  -- version 2011-01-06
%   r ... original data
%   n ... length of bootstrap sample x
%   b ... block length

if nargin < 3, b = 1; end
[T k] = size(r);
if b==1  % simple bootstrap
    j = ceil(rand(n,1)*T);
    x = r(j,:);
else   % circular block bootstrap
    nb = ceil(n/b);            % number of bootstraps
    js = floor(rand(nb,1)*T);  % starting points - 1
    x = nan(nb*b,k);
    for i = 1:nb
        j = mod(js(i)+(1:b), T)+1; % positions in original data
        s = (1:b) + (i-1)*b;
        x(s,:) = r(j,:);
    end
    if (nb*n) > n               % correct length if nb*b > n
        x((n+1):end,:) = [];
    end
end