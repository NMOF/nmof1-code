function f_x = KDE(x,xi,h,Kernel)
% KDE.m  -- version 2011-01-06
%   kernel density estimation
%   x ... points at which to estimate the density
%   xi .. original sample
%   h ... bandwidth, smoothing parameter
%   Kernel .. name of kernel

if nargin < 4  % default function
    Kernel = 'Gaussian';
    if nargin < 3  % default value for h
        h = (4/(3*length(xi)))^.2 * std(xi);
    end
end

switch upper(Kernel)
    case 'GAUSSIAN',
        K = @(y) exp(-y.^2/2) / sqrt(2*pi);
    case 'UNIFORM',
        K = @(y) (abs(y) <1 )/2;
    case 'TRIANGULAR',
        K = @(y) max(0,(1-abs(y)));
    case {'QUADRATIC','EPANECHNIKOV'}
        K = @(y) max(0, 0.75*(1-y.^2)) ;
    otherwise
        error('Kernel not recognised')
end

f_x = zeros(size(x));
n = length(xi);
for j = 1:length(x)
    y = (x(j) - xi(:))/h;
    f_x(j) = ones(1,n) * K(y) / (h*n) ;
end