% equations.m -- version 2011-01-02
m = 10000; n = 10; trials = 100;  
X = randn(m,n); y = randn(m,1);

% QR
tic
for r=1:trials
    sol1 = X\y;
end
toc

% form (X'X) and (X'y)
tic
for r=1:trials
    sol2 = (X'*X)\(X'*y);
end
toc

% check
max(abs(sol1(:)-sol2(:)))
