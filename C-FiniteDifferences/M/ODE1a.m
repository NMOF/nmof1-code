% ODE1a.m  -- version 1997-12-09
f7 =  1;
x0 =  2; y0 = 1;
xN = 10; N  = 10;
dx = (xN - x0)/N;
x  =  linspace(x0,xN,N+1); y = zeros(1,N+1);
y(f7+0) = y0;
for i = 1:N
    y(f7+i) = y(f7+i-1) * ( 1 - dx/x(f7+i-1) );
end
