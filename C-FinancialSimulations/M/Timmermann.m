function [S_RE,Div,g_est,S_FT] = Timmermann(T,r,mu,sigma,n)
% Timmermann.m  -- version 2011-01-06
g_true = mu + randn(T+n,1) * sigma;
mu_hat = zeros(T+n,1);   var_hat = zeros(T+n,1);
mu_hat(n+1,1)  = mean(g_true(1:n));
var_hat(n+1,1) = var(g_true(1:n));
log_div(n+1,1) = 0;
weight = (n-1)/n;
for t = (n+2):(T+n);
    mu_hat(t)  = weight * mu_hat(t-1)  + (1/n) * g_true(t);
    var_hat(t) = weight * var_hat(t-1) + (1/n) * ( weight * ...
                 ( g_true(t)-mu_hat(t-1))^2);
    log_div(t) = log_div(t-1) + g_true(t);
end;
% -- discard first n observation (preceeding observations)
mu_hat(1:n)  = [];   var_hat(1:n) = [];  log_div(1:n) = [];
% -- compute prices
Div   = exp(log_div);
g_est = exp(mu_hat + var_hat/2);
g_est = min(g_est,(1 + r -.0001));
% -- price under rational expectations
S_RE = Div .* (g_est ./ (1+r-g_est));
% -- fundamental price under known true parameters
S_FT = Div .* (exp(mu + sigma^2/2)/(1+r-exp(mu+sigma^2/2)));
