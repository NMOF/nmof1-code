function [r,e] = MAsim(T,sigma,theta);
% MAsim.m  -- version 2011-01-06
%   simulation of MA(q) process
q = length(theta);
e = randn(T+q,1) * sigma;
r = zeros(T+q,1);
for t = q+(1:T)
    r(t) = mu + e(t-(0:q))' * [1; theta(:)];
end
r(1:q) = [];
e(1:q) = [];

