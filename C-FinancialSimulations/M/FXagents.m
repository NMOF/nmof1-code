% FXagents.m  -- version 2011-01-06
% -- parameters
N_agents = 50;   % number of agents
N_days   = 500; % number of days
N_IPD    = 20;   % number of interactions per day

P_fund   = 100;  % fundamental price
sigma_p  = 0.1;  % additional price volatility

g        = 1;    % adj. speed chartists; 0 < g <=1
nu       = .01;  % adj. speed fundamentalisits;   0 < nu <= 1
delta    = .25;  % probbility convincing
epsilon  = .01;  % random type change    
    
% -- initial setting
N_I    = N_days * N_IPD;  % total number of interactions
isFund = rand(N_agents,1)<.5; % type of investor
P      = nan(N_I,1);      % intra period prices; initial values
P(1:2) = P_fund + randn(2,1)*sigma_p;
w = nan(N_I,1);           % perceived fraction of fundamentalists

% -- emergence over time    
for i = 3:N_I
    a = randperm(N_agents);
    if rand < delta ,  % recruitment
        isFund(a(2)) = isFund(a(1)); 
    end

    if rand < epsilon, % individual change of opinion
        isFund(a(3)) = ~isFund(a(3)); 
    end;

    w(i) = mean(isFund);  % perceived fraction of fundamentalists

    % expected price changes and new price
    E_change_F(i) = (P_fund - P(i-1)) * nu;
    E_change_C(i) = (P(i-1)-P(i-2)) * g;
    change = w(i) * E_change_F(i)  + (1-w(i)) * E_change_C(i);
    P(i) = abs((P(i-1) + change) +  randn*sigma_p);
end;

% -- extract end of period prices 
t = N_IPD:N_IPD:N_I;
S = P(t);
