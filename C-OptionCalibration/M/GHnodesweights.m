function [x,w] = GHnodesweights(n)
% GHnodesweights.m  -- version 2010-11-07
% (G_auss H_ermite...) 
eta = sqrt((1:(n-1)) / 2);
A = diag(eta,1) + diag(eta,-1);       
[V,D] = eig(A);
x = diag(D);
% Matlab does not guaranty sorted eigenvalues
[x,i] = sort(x); 
% weights: for Hermite, integral from -inf to inf = sqrt of pi
w = sqrt(pi) * V(1,i) .^ 2; 

