% example.m -- version 2010-10-24
%% example BSM
S       = 100;      % spot price
q       = 0.08;     % dividend yield (eg, 0.03)
r       = 0.02;     % interest rate (eg, 0.03)
X       = 100;      % strike
tau     = 1;        % time to maturity
v       = 0.2^2;    % variance
clc
tic, call = callBSMcf(S,X,tau,r,q,v); t=toc;
fprintf('BSM\nwith CF:         %6.3f, required time: %4.3f seconds\n',call,t)

tic, call = callBSM(S,X,tau,r,q,v);t=toc;
fprintf('classic formula: %6.3f, required time: %4.3f seconds\n---\n\n',call,t)

%% example Heston
S       = 100;
q       = 0.08;
r       = 0.02; 
X       = 100;
tau     = 1;
k       = 1.0;      % mean reversion speed (kappa in paper)
sigma   = 0.00001;      % vol of vol
rho     = -0.7;     % correlation
v0      = 0.2^2;    % current variances
vT      = 0.2^2;    % long-run variance (theta in paper)

tic, call = callHestoncf(S,X,tau,r,q,v0,vT,rho,k,sigma); t=toc;
fprintf('Heston\nwith CF:         %6.3f, required time: %4.3f seconds\n---\n\n',call,t)

%% example Bates
S       = 100;
q       = 0.08;
r       = 0.02; 
X       = 100;
tau     = 1;
k       = 1.0;      % mean reversion speed (kappa in paper)
sigma   = 0.00001;  % vol of vol
rho     = -0.3;     % correlation
v0      = 0.2^2;    % current variances
vT      = 0.2^2;    % long-run variance (theta in paper)
lambda  = 0.0;      % intensity of jumps;
muJ     = -0.0;     % mean of jumps;
vJ      = 0.00001^2;    % variance of jumps;

tic, call = callBatescf(S,X,tau,r,q,v0,vT, ...
                        rho,k,sigma,lambda,muJ,vJ); t=toc;
fprintf('Bates\nwith CF:         %6.3f, required time: %4.3f seconds\n---\n\n',call,t)

%% example Merton jump--diffusion
S       = 100;
q       = 0.08;
r       = 0.02; 
X       = 100;
tau     = 1;
v       = 0.2^2;    % variance (volatility squared)
lambda  = 0.2;      % intensity of jumps;
muJ     = -0.1;     % mean of jumps;
vJ      = 0.2^2;    % variance of jumps;
N       = 20;       % number of jumps for classic formula

tic, call = callMertoncf(S,X,tau,r,q,v,lambda,muJ,vJ); t = toc;
fprintf('Merton jump-diffusion\nwith CF:         %6.3f, required time: %4.3f seconds\n',call,t)

tic, call = callMerton(S,X,tau,r,q,v,lambda,muJ,vJ,N); t = toc;
fprintf('classic formula: %6.3f, required time: %4.3f seconds\n',call,t)