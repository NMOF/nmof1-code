% exampleQuad1.m -- version 2010-10-24
%% Riemann sums - example
Fun1 = @(x)(exp(-x));
m = 5; a = 0; b = 5; h = (b-a)/m;

% rectangular rule -- left
w = h; k = 0:(m-1); x = a + k * h;
fprintf('rectangular (left)  with %i rectangles:\t %f\n',m,sum(w * Fun1(x)))

% rectangular rule -- right
w = h; k = 1:m; x = a + k * h;
fprintf('rectangular (right) with %i rectangles:\t %f\n',m,sum(w * Fun1(x)))

%midpoint rule
w = h; k = 0:(m-1); x = a + (k + 0.5)*h;
fprintf('midpoint            with %i rectangles:\t %f\n',m,sum(w * Fun1(x)))

%trapezoidal rule
w = h; k = 1:(m-1); x = [a a + k*h b];
aux = w * Fun1(x); aux([1 end]) = aux([1 end])/2;
fprintf('trapezoidal         with %i rectangles:\t %f\n',m,sum(aux))

%adaptive Simpson
fprintf('Adaptive Simpson (Matlab):\t\t\t\t %f\n',quad(Fun1,a,b))