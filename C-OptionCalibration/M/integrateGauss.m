% integrateGauss.m -- version 2010-10-24
% goal: to compute N(b)
b = 2;

% number of nodes
n = 25;         
% replace minus infinity by ...
lowerLim = -10; 
% compute nodes/weights
[x,w] = GLnodesweights(n);
% change interval of integration
[x,w] = changeInterval(x, w, -1, 1, lowerLim, b);

% result of integration
ourResult = w*GaussF(x)
% result of normcdf from Statistics Toolbox
MatlabResult = normcdf(b)

abs(ourResult-MatlabResult)
