function b = solve3diag(l,u,q,b)
% solve3diag.m  -- version 1999-05-11
% Back and forward substitution for tridiagonal system
n = length(b);
for i = 2:n
    b(i) = b(i) - l(i-1) * b(i-1);
end
b(n) = b(n) / u(n);
for i = n-1:-1:1
    b(i) = ( b(i) - q(i) * b(i+1) ) / u(i);
end

