% spearman.m  version -- 2011-01-10
Y = randn(20,1);
Z = randn(20,1);
corr(Y,Z,'type','Spearman')
% 
[ignore, indexY] = sort(Y);
[ignore, indexZ] = sort(Z);
[ignore, ranksY] = sort(indexY);
[ignore, ranksZ] = sort(indexZ);
corr(ranksY,ranksZ,'type','Pearson')